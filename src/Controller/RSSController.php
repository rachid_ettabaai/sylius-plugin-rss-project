<?php

declare(strict_types=1);

namespace Acme\SyliusExamplePlugin\Controller;

use App\Entity\Product\Product;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

final class RSSController extends Controller
{
    /**
     *  Get Rss feed
     * @return Response
     */
    public function feedAction(): Response
    {
        $products = $this->getDoctrine()->getRepository(Product::class)->findAll();
        $content = [];
        foreach ($products as $product) {
            $prodarray = [
                'product_id' => $product->getId(),
                'product_code' => utf8_encode($product->getCode()),
                'product_slug' => utf8_encode($product->getSlug()),
                'product_name' => utf8_encode($product->getName()),
                'product_shortdescription' => utf8_encode($product->getShortDescription()),
                'product_description' => utf8_encode($product->getDescription())
            ];
            array_push($content,$prodarray);
        }
        //dump($content);
        $encoders = [new XmlEncoder()];
        $normalizers = [new ObjectNormalizer()];

        $serializer = new Serializer($normalizers, $encoders);

        $rssfeed = $serializer->serialize($content,'xml');

        $response = new Response();
        $response->headers->set('Content-Type', 'text/xml;charset=UTF-8');
        $response->setContent($rssfeed);

        return $response;
        
    }

}
